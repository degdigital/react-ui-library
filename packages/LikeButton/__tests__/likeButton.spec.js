/***/
import React from 'react';
import { LikeButton }  from "../index";
import { render, screen, fireEvent } from "../../../test/utils/testUtilities";
import '@testing-library/jest-dom/extend-expect';

describe('Like Button', () => {
  it('renders without crashing', () => {
    render(<LikeButton/>);
    expect(screen.getByLabelText('like-me-btn')).toBeInTheDocument();
  });
  it('has proper button text', () => {
    render(<LikeButton/>);
    expect(screen.getByRole('button')).toHaveTextContent('Click me!');
  });
  it('renders the initial string', () => {
    render(<LikeButton/>);
    expect(screen.getByText('You HATE me')).toBeInTheDocument();
  });
  it('changes message when button is clicked', () => {
    render(<LikeButton/>);
    fireEvent.click(screen.getByLabelText('like-me-btn'));
    expect(screen.getByText('You Like me')).toBeInTheDocument();
  })
});
