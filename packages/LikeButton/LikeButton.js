import React, { useState } from 'react';

const LikeButton = () => {
  const [like, setLike] = useState(false);
  let string = '';
  like ? string = "You Like me" : string = "You HATE me";
  return (
    <div>
      <button onClick={() => setLike(!like)} className="like-me-btn" aria-label="like-me-btn">Click me!</button>
      <p>{string}</p>
    </div>
  );
};

export default LikeButton;
