/**
 * Renders markdown docs in a React component
 */
import React, { Component } from "react";
import PropTypes from 'prop-types';
import ReactMarkdown from "react-markdown/with-html";
import { MarkdownCodeBlock } from "./index";

/**
 * Set state from props. If props variables aren't present, pass defaults
 * @param {Object} props props passed down from React
 * @property {String} Object.markdown to be rendered
 * @property {String} Object.mdFilePath relative path to the markdown file
 * @property {Boolean} Object.escapeHtml flag to escape HTML in markdown
 * @property {Boolean} Object.error error flag if async failure
 * @return {Object}
 */
const getInitState = (props) => {
  let markdown = props.markdown ? props.markdown : '';
  let mdFilePath = props.mdFilePath ? props.mdFilePath : '';
  let escapeHtml = props.escapeHtml ? props.escapeHtml : false;
  return {
    markdown,
    mdFilePath,
    escapeHtml,
    error: false
  }
};

class MarkdownRenderer extends Component {
  constructor(props) {
    super(props);
    this.state = getInitState(props);
  }
  
  static propTypes = {
    mdFilePath: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool
    ]),
    escapeHtml: PropTypes.bool
  };
  
  /**
   * Get contents of Markdown file
   * If no markdown file has been passed down, we simply return
   */
  async componentDidMount() {
    if (this.state.mdFilePath === '') return;
    try {
      const response = await fetch(`${this.state.mdFilePath}`);
      const text = await response.text();
      if (!response.ok) {
        throw Error(response.statusText);
      }
      this.setState({ markdown: text })
    } catch (e) {
      console.log("error", e);
      this.setState({ error: true })
    }
  }
  
  render() {
    const { markdown } = this.state;
    
    return (
      <>
        {this.state.error && <div className='error-in-async'><p><b>There was an error fetching data</b></p></div> }
        {!this.state.error &&
          <ReactMarkdown
            source={markdown}
            renderers={{ code: MarkdownCodeBlock }}
            escapeHtml={this.props.escapeHtml}
          />
        }
      </>
    );
  }
}

export default MarkdownRenderer;
