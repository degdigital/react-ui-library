import React, { Component } from "react";
import PropTypes from 'prop-types';
import hljs from 'highlight.js';
import "./darcula.css";

/**
 * Set init state
 * @param {Object} props object from React
 * @property {*} codeEl @todo find out what this is
 * @property {String} value
 * @property {String} language
 */
const setInitState = (props) => {
  let codeEl, value, language;
  codeEl = '';
  props.value ? value = props.value : value = '';
  props.language ? language = props.language : language = '';
  return {
    codeEl,
    value,
    language
  }
};

class MarkdownCodeBlock extends Component {
  constructor(props) {
    super(props);
    this.state = setInitState(props);
    this.setRef = this.setRef.bind(this);
  }
  static propTypes = {
    codeEl: PropTypes.any,
    value: PropTypes.string,
    language: PropTypes.string
  };
  
  setRef(el) {
    this.setState({ codeEl: el });
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    this.highlightCode();
  }
  
  highlightCode = () => {
    hljs.highlightBlock(this.state.codeEl);
    document.querySelectorAll('pre code').forEach(block => {
      hljs.highlightBlock(block);
    });
  };
  
  render() {
    return (
      <pre>
        <code ref={this.setRef} className={`language-${this.props.language}`}>
          {this.props.value}
        </code>
      </pre>
    )
  }
}

export default MarkdownCodeBlock;
