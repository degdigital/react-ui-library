const path = require('path');

module.exports = {
  stories: [
    '../packages/**/*.stories.(js|mdx)'
  ],
  addons: [
    '@storybook/addon-a11y',
    '@storybook/addon-actions',
    '@storybook/addon-backgrounds',
    {
      name: '@storybook/addon-docs',
      options: { configureJSX: true }
    },
    '@storybook/addon-knobs',
    '@storybook/addon-links',
    '@storybook/addon-options',
    '@storybook/addon-storysource',
    '@storybook/addon-viewport'
  ],
  webpackFinal: async config => {
    // do mutation to the config
    // The following hacks were found here -> https://github.com/storybookjs/storybook/issues/3346#issuecomment-459439438
    // User real file paths for symlinked dependencies to avoid including them multiple times
    config.resolve.symlinks = true;
    // HACK: extend existing JS rule to ensure all dependencies are correctly ignored
    // https://github.com/storybooks/storybook/issues/3346#issuecomment-459439438
    const jsRule = config.module.rules.find((rule) => rule.test.test('.jsx'));
    jsRule.exclude = /node_modules/;
  
    // HACK: Instruct Babel to check module type before injecting Core JS polyfills
    // https://github.com/i-like-robots/broken-webpack-bundle-test-case
    const babelConfig = jsRule.use.find(({ loader }) => loader === 'babel-loader');
    babelConfig.options.sourceType = 'unambiguous';
    // babelConfig.options.plugins.push("@babel/plugin-proposal-class-properties");
  
    // HACK: Ensure we only bundle one instance of React
    config.resolve.alias.react = require.resolve('react');
    
    config.module.rules.push({
      test: /\.css$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true,
          }
        }
      ],
      include: path.resolve(__dirname, '../')
    });
    
    return config;
  },
  /*webpackFinal: (config) => console.dir(config, { depth: null }) || config,*/
};
