import { addParameters, addDecorator } from "@storybook/react";
import { DocsPage, DocsContainer } from "@storybook/addon-docs/dist/blocks";
import React from "react";
/**
 * This configuration is necessary to make addon-docs work
 * with React
 */
addParameters({
  docs: {
    container: DocsContainer,
    page: DocsPage
  }
});

/**
 * Adds some spacing so that elements aren't butted up against the edges
 */
addDecorator(story => <div style={{ padding: '40px', backgroundColor: '#F8F8FF' }} className='story-wrapper'>{story()}</div>);
