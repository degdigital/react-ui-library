# Markdown Code Highlighting

## Javascript
```javascript
var two = 1;
var b = 2;
function(a, b) {
  return a + b;
}
```

## C++
```c++
#include <iostream>
using namespace std;

// main() is where program execution begins
int main() {
    cout << "Hello World"; // print
    return 0;
}
```

## Java
```java
public class MyClass {
  public static void main(String[] args) {
    System.out.println("Hello World");
  }
}
```
## CSS
```css
.main {
  background-color: #998999;
}
```

