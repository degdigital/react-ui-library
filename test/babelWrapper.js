/**
 * This function sets babels "rootMode" to upward
 * so that it searches for the root config file.
 * @link https://babeljs.io/docs/en/config-files
 */
module.exports = require('babel-jest').createTransformer({
  rootMode: "upward"
});
